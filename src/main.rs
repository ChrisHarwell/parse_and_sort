use std::collections::HashSet;
use std::fs::File;
use std::io::{self, BufReader, BufWriter, Write, BufRead};
use std::path::Path;

fn main() -> io::Result<()> {
    let path = Path::new("PRs_To_Review.md");
    let file = File::open(&path)?;
    let reader = BufReader::new(file);

    let substrings = vec![
        "external-api-model",
        "mastery-ingress-api",
        "external-api-egress",
        "api-developer-portal",
        "mastery-ingress-driver-consumer",
        "mastery-ingress-trailer-consumer",
        "mastery-ingress-power-consumer",
    ];

    let mut seen = HashSet::new();
    let mut top_lines: Vec<String> = Vec::new();
    let mut bottom_lines: Vec<String> = Vec::new();

    for line in reader.lines() {
        let line = line?;
        if seen.contains(&line) {
            continue;
        }
        seen.insert(line.clone());
        
        if substrings.iter().any(|substring| line.contains(substring)) {
            bottom_lines.push(line);
        } else {
            top_lines.push(line);
        }
    }

    top_lines.sort();
    bottom_lines.sort();

    let mut file = BufWriter::new(File::create(&path)?);
    for line in top_lines {
        writeln!(file, "{}", line)?;
    }

    writeln!(file, "\n\n\n")?;

    for line in bottom_lines {
        writeln!(file, "{}", line)?;
    }

    Ok(())
}
