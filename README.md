# Rust PRs To Review Sorter

This Rust program reads a markdown file named `PRs_To_Review.md` that contains a list of Pull Requests (PRs) to be reviewed. It categorizes the PRs into two sections based on if the PR is related to a specific subset of projects (listed in the code) or not. It then sorts each section alphabetically and rewrites the file with the sorted list, maintaining the two categories.

## Preparing the PRs_To_Review.md File

Before running this Rust program, you should prepare your `PRs_To_Review.md` file by copying and pasting text from Slack to get all of the links[^1] to PRs and then format the file using the following `sed` command:

```bash
cd src && sed -Ei '' -n -e '/https:\/\/github.com\/masterysystems\/[^/]+\/pull\/[0-9]+/ s@.*@- [&]@p' PRs_To_Review.md
```

This command will extract all the GitHub PR review links and format them properly in the `PRs_To_Review.md` file.

## How to Run

You need to have Rust installed on your machine. If you haven't, please visit [Rust Installation](https://www.rust-lang.org/tools/install) for the installation guide.

Once Rust is installed, you can clone this repository and navigate into the directory.

```bash
# With HTTPS
git clone https://gitlab.com/ChrisHarwell/parse_and_sort.git && cd parse_and_sort
```

```bash
# With SSH
git clone git@gitlab.com:ChrisHarwell/parse_and_sort.git && cd parse_and_sort
```

Next, you can run the program with:

```bash
cd src && cargo run
```

This command will read the `PRs_To_Review.md` file, sort the PRs into the mentioned categories, and rewrite the file.

## Features

- Sorting PRs: This program can sort a list of PRs alphabetically.
- Categorizing PRs: PRs are categorized into two sections - those related to the listed subset of projects and the rest. Each section is sorted individually.
- Deduplication: The program also removes any duplicate lines.

## How it works

The script reads the `PRs_To_Review.md` file line by line. It checks each line (PR) against a predefined list of projects:

- "external-api-model"
- "mastery-ingress-api"
- "external-api-egress"
- "api-developer-portal"
- "mastery-ingress-driver-consumer"
- "mastery-ingress-trailer-consumer"
- "mastery-ingress-power-consumer"

If a PR (line) is related to any of the above projects, it is categorized into the second category (bottom lines). All other PRs are placed into the first category (top lines).

Once all lines are read and categorized, they are sorted individually, and the file `PRs_To_Review.md` is rewritten with the sorted PRs, maintaining the two categories.

## Contribute

If you'd like to contribute, please fork the repository and use a feature branch. Pull requests are warmly welcome.

## License

This project is free software: you can redistribute it and/or modify it under the terms of the MIT License. See the `LICENSE.md` file for more details.

[^1]: NOTE: Copy the entire blurb of text, the SED command will remove everything that isn't a GitHub PR Link
